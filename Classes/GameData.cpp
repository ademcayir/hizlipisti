#include "GameData.h"
#include "GameCard.h"

USING_NS_CC;

GameData* GameData::instance = nullptr;

GameData::GameData()
{
    this->loadGame();
}

GameData* GameData::getInstance()
{
    if (instance == nullptr)
        instance = new GameData();
    return instance;
}

void GameData::clearVectors() {
    this->deckClosedCards.clear();
    this->deckCollectedCards.clear();
    this->playerCards.clear();
    this->computerCards.clear();
    this->deckCards.clear();
}

void GameData::loadGame() {
    clearVectors();
    auto data = UserDefault::getInstance();
    auto d = data->getDataForKey("GAME");
    unsigned char* buf = d.getBytes();
    ssize_t byte_len = d.getSize();
    if (buf != nullptr && byte_len == 60){
        int len=0;
        int size = buf[len++];
        for (int i=0; i<size; i++) {
            auto card = new GameCard();
            card->loadInt(buf[len++]);
            deckClosedCards.push_back(card);
        }
        size = buf[len++];
        for (int i=0; i<size; i++) {
            auto card = new GameCard();
            card->loadInt(buf[len++]);
            deckCollectedCards.push_back(card);
        }
        size = buf[len++];
        for (int i=0; i<size; i++) {
            auto card = new GameCard();
            card->loadInt(buf[len++]);
            playerCards.push_back(card);
        }
        size = buf[len++];
        for (int i=0; i<size; i++) {
            auto card = new GameCard();
            card->loadInt(buf[len++]);
            computerCards.push_back(card);
        }
        size = buf[len++];
        for (int i=0; i<size; i++) {
            auto card = new GameCard();
            card->loadInt(buf[len++]);
            deckCards.push_back(card);
        }
        gameState = (GameState)buf[len++];
        playerScore = buf[len++];
        computerScore = buf[len++];
    } else {
        this->newGame();
    }
}

void GameData::saveState() {
    unsigned char buf[1000];
    int len = 0;
    buf[len++] = deckClosedCards.size();
    for (int i=0; i<deckClosedCards.size(); i++) {
        buf[len++] = deckClosedCards.at(i)->getInt();
    }
    buf[len++] = deckCollectedCards.size();
    for (int i=0; i<deckCollectedCards.size(); i++) {
        buf[len++] = deckCollectedCards.at(i)->getInt();
    }
    buf[len++] = playerCards.size();
    for (int i=0; i<playerCards.size(); i++) {
        buf[len++] = playerCards.at(i)->getInt();
    }
    buf[len++] = computerCards.size();
    for (int i=0; i<computerCards.size(); i++) {
        buf[len++] = computerCards.at(i)->getInt();
    }
    buf[len++] = deckCards.size();
    for (int i=0; i<deckCards.size(); i++) {
        buf[len++] = deckCards.at(i)->getInt();
    }
    buf[len++] = (int)gameState;
    buf[len++] = playerScore;
    buf[len++] = computerScore;
    auto data = UserDefault::getInstance();
    Data d;
    d.copy(buf, len);
    data->setDataForKey("GAME", d);
}

void GameData::clearSaving() {
    auto data = UserDefault::getInstance();
    Data d;
    data->setDataForKey("GAME", d);
}


int myrandom (int i) { return std::rand()%i;}


void GameData::newGame() {
    std::vector<int> cards;
    for (int i=0; i<52; i++) {
        cards.push_back(i);
    }
    std::random_shuffle(cards.begin(), cards.end(),myrandom);
    this->clearVectors();
    for (int i=0; i<cards.size(); i++) {
        GameCard* card = new GameCard();
        card->loadInt(cards[i]);
        this->deckClosedCards.push_back(card);
    }
    for (int i=0; i<4; i++) {
        GameCard* card1 = this->deckClosedCards.back();
        this->deckClosedCards.pop_back();
        GameCard* card2 = this->deckClosedCards.back();
        this->deckClosedCards.pop_back();
        GameCard* card3 = this->deckClosedCards.back();
        this->deckClosedCards.pop_back();
        this->playerCards.push_back(card1);
        this->computerCards.push_back(card2);
        this->deckCards.push_back(card3);
    }
    
    if (rand() % 2 == 0){
        this->gameState = GameState::PlayerTurn;
    } else {
        this->gameState = GameState::ComputerTurn;
    }
    this->gameState = GameState::ComputerTurn;
    computerScore = 0;
    playerScore = 0;
    saveState();
}

void GameData::playerCardPlayed(GameCard* card) {
    for (int i=0; i<playerCards.size(); i++) {
        if (playerCards.at(i) == card) {
            playerCards.erase(playerCards.begin() + i);
            break;
        }
    }
    deckCards.push_back(card);
    int checkCard = this->checkDeckCard();
    if (checkCard != -1) {
        this->playerScore += checkCard;
        for (int i=0; i < this->deckCards.size(); i++) {
            this->deckCollectedCards.push_back(this->deckCards.at(i));
        }
        this->deckCards.clear();
        this->gameState = GameState::PlayerWonHand;
    } else {
        this->gameState = GameState::ComputerTurnStart;
    }
    saveState();
}

void GameData::computerCardPlayed(GameCard* card) {
    for (int i=0; i<computerCards.size(); i++) {
        if (computerCards.at(i) == card) {
            computerCards.erase(computerCards.begin() + i);
            break;
        }
    }
    deckCards.push_back(card);
    int checkCard = this->checkDeckCard();
    if (checkCard != -1) {
        this->computerScore += checkCard;
        for (int i=0; i < this->deckCards.size(); i++) {
            this->deckCollectedCards.push_back(this->deckCards.at(i));
        }
        this->deckCards.clear();
        this->gameState = GameState::ComputerWonHand;
    } else {
        this->gameState = GameState::PlayerTurnStart;
    }
    saveState();
}


GameCard* GameData::computerCard() {
    if (computerCards.size() == 0)
        CCLOGERROR("computer does not have card!!\n");
    // tek eleman varsa direk at
    if (computerCards.size() == 1)
        return computerCards.at(0);
    // ayni kart ara. varsa onu at
    GameCard* retval = nullptr;
    if (this->deckCards.size() > 0){
        GameCard* card = this->deckCards.back();
        for (int i=0; i < computerCards.size(); i++) {
            if (computerCards.at(i)->num == card->num){
                retval = computerCards.at(i);
                break;
            }
        }
    }
    // ayni kart yoksa, j olmayani at
    if (retval == nullptr) {
        for (int i=0; i < computerCards.size(); i++) {
            if (computerCards.at(i)->num != CardNum::Jack){
                retval = computerCards.at(i);
            }
        }
    }
    // eger hala bulamadiysa ilk elemani yolla
    if (retval == nullptr)
        retval = computerCards.at(0);
    return retval;
}
void GameData::nextState() {
    if (this->gameState == GameState::PlayerTurn) {
        
    } else if (this->gameState == GameState::PlayerTurnStart) {
        if (playerCards.size() == 0) {
            this->gameState = GameState::PlayerNoCardInHand;
        } else {
            this->gameState = GameState::PlayerTurn;
        }
    } else if (this->gameState == GameState::ComputerTurnStart) {
        if (computerCards.size() == 0) {
            this->gameState = GameState::ComputerNoCardInHand;
        } else {
            this->gameState = GameState::ComputerTurn;
        }
    } else if (this->gameState == GameState::PlayerNoCardInHand) {
        if (this->deckClosedCards.size() == 0) {
            this->gameState = GameState::CardsEnd;
        } else {
            for (int i=0; i<4; i++) {
                GameCard* card1 = deckClosedCards.back();
                deckClosedCards.pop_back();
                GameCard* card2 = deckClosedCards.back();
                deckClosedCards.pop_back();
                playerCards.push_back(card1);
                computerCards.push_back(card2);
            }
            this->gameState = GameState::PlayerTurn;
        }
    } else if (this->gameState == GameState::ComputerNoCardInHand) {
        if (this->deckClosedCards.size() == 0) {
            this->gameState = GameState::CardsEnd;
        } else {
            for (int i=0; i<4; i++) {
                GameCard* card1 = deckClosedCards.back();
                deckClosedCards.pop_back();
                GameCard* card2 = deckClosedCards.back();
                deckClosedCards.pop_back();
                playerCards.push_back(card1);
                computerCards.push_back(card2);
            }
            this->gameState = GameState::ComputerTurn;
        }
    } else if (this->gameState == GameState::ComputerWonHand) {
        this->gameState = GameState::PlayerTurnStart;
    } else if (this->gameState == GameState::PlayerWonHand) {
        this->gameState = GameState::ComputerTurnStart;
    }
    saveState();
}




int GameData::checkDeckCard() {
    int retval = -1;
    if (deckCards.size() > 1){
        GameCard* lastCard = deckCards.at(deckCards.size() - 1);
        GameCard* deckCard = deckCards.at(deckCards.size() - 2);
        if (lastCard->num == deckCard->num) {
            retval = 0;
            if (deckCards.size() == 2){
                if (lastCard->num == CardNum::Jack){
                    retval += 20;
                } else {
                    retval += 10;
                }
            }
        } else if (lastCard->num == CardNum::Jack) {
            retval = 0;
        }
        
        if (retval != -1) {
            for (int i=0; i < deckCards.size(); i++) {
                GameCard* card = deckCards.at(i);
                if (card->color == CardColor::Clubs && card->num == CardNum::Card2) {
                    retval += 2;
                } else if (card->color == CardColor::Diamonds && card->num == CardNum::Card10) {
                    retval += 10;
                } else if (card->num == CardNum::Jack) {
                    retval += 1;
                } else if (card->num == CardNum::Ace) {
                    retval += 1;
                }
            }
        }
    }
    return retval;
}




