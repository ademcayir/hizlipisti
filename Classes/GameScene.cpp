#include "GameScene.h"
#include "GameData.h"
#include "GameCardSprite.h"
#include "GameCard.h"
#include "MenuScene.h"


#define ANIM_TIME 0.3

USING_NS_CC;

Scene* GameScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() ) {
        return false;
    }
    this->computerName = nullptr;
    this->playerName = nullptr;
    this->deckCard = nullptr;
    this->closedCard = nullptr;
    this->gameoverLayer = nullptr;
    this->menuItem = nullptr;
    this->menuLayer = nullptr;
    this->upIcon = nullptr;
    this->downIcon = nullptr;
    this->fontSize = 100;
    this->playerCards.clear();
    this->computerCards.clear();
    GameData::getInstance()->loadGame();
    auto background = LayerColor::create(Color4B(10, 200, 10, 255));
    addChild(background,0);
    this->renderDeck();
    this->checkGameState();
    return true;
}


void GameScene::checkGameState() {
    if (GameData::getInstance()->gameState == GameState::ComputerTurnStart) {
        GameData::getInstance()->nextState();
        checkGameState();
    } else if (GameData::getInstance()->gameState == GameState::PlayerTurnStart) {
        GameData::getInstance()->nextState();
        checkGameState();
    } else if (GameData::getInstance()->gameState == GameState::ComputerCardDealing) {
        
    } else if (GameData::getInstance()->gameState == GameState::ComputerNoCardInHand) {
        CCLOG("Computer dealing cards");
        GameData::getInstance()->nextState();
        checkGameState();
    } else if (GameData::getInstance()->gameState == GameState::PlayerNoCardInHand) {
        CCLOG("Player dealing cards");
        GameData::getInstance()->nextState();
        checkGameState();
    } else if (GameData::getInstance()->gameState == GameState::PlayerWonHand) {
        CCLOG("Player won hand");
        GameData::getInstance()->nextState();
        checkGameState();
    } else if (GameData::getInstance()->gameState == GameState::ComputerWonHand) {
        CCLOG("Computer won hand");
        GameData::getInstance()->nextState();
        checkGameState();
    } else if (GameData::getInstance()->gameState == GameState::ComputerTurn) {
        this->renderDeck();
        
        GameCardSprite* throwingCard = nullptr;
        for (int i = computerCards.size() - 1; i >= 0 ; i--) {
            if (computerCards.at(i)->isVisible()) {
                throwingCard = computerCards.at(i);
                break;
            }
        }
        
        if (throwingCard != nullptr) {
            auto delay = DelayTime::create(2);
            auto card = GameData::getInstance()->computerCard();
            auto callback = CallFunc::create([this,card,throwingCard](){
                if (throwingCard != nullptr) {
                    throwingCard->setCard(card);
                }
            });
            auto move = MoveTo::create(ANIM_TIME,
                                       Vec2(deckCard->getPosition().x - deckCard->getContentSize().width,
                                            deckCard->getPosition().y + deckCard->getContentSize().height / 2));
            auto callbackEnd = CallFunc::create([this,card,throwingCard](){
                GameData::getInstance()->computerCardPlayed(card);
                this->checkGameState();
                throwingCard->loadDefaultPosition();
                throwingCard->setCard(nullptr);
            });
            auto seq = Sequence::create(delay,callback,move,callbackEnd, NULL);
            throwingCard->runAction(seq);
        }
    } else if (GameData::getInstance()->gameState == GameState::CardsEnd) {
        this->showGameover();
    }
    renderDeck();
    
}

void GameScene::renderDeck() {
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    
    if (this->menuItem == nullptr){
        this->menuItem = MenuItemImage::create("res/menu_button.png", "res/menu_button.png",CC_CALLBACK_1(GameScene::menuTapped, this));
        this->menuItem->setAnchorPoint(Vec2(1,1));
        this->menuItem->setPosition(Vec2(origin.x + visibleSize.width - fontSize, origin.y + visibleSize.height - fontSize));
        // create menu, it's an autorelease object
        auto menu = Menu::create(this->menuItem, NULL);
        menu->setPosition(Vec2::ZERO);
        addChild(menu, 100);
    }
    
    if (computerName == nullptr) {
        computerName = Label::create();
        computerName->setSystemFontSize(fontSize);
        computerName->setAnchorPoint(Vec2(0.5, 1));
        computerName->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height - fontSize/2));
        addChild(computerName,1);
    }
    if (playerName == nullptr) {
        playerName = Label::create();
        playerName->setSystemFontSize(fontSize);
        playerName->setAnchorPoint(Vec2(0.5, 0));
        playerName->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + fontSize/2));
        addChild(playerName,1);
    }
    
    if (closedCard == nullptr ) {
        closedCard = GameCardSprite::create();
        closedCard->setCard(nullptr);
        closedCard->setAnchorPoint(Vec2(0, 0.5));
        closedCard->setPosition(Vec2(origin.x + visibleSize.width/2 + closedCard->getContentSize().width/4, origin.y + visibleSize.height/2));
        addChild(closedCard,1);
    }
    
    if (deckCard == nullptr ) {
        deckCard = GameCardSprite::create();
        deckCard->setCard(nullptr);
        deckCard->setAnchorPoint(Vec2(1, 0.5));
        deckCard->setPosition(Vec2(origin.x + visibleSize.width/2 - deckCard->getContentSize().width/4, origin.y + visibleSize.height/2));
        addChild(deckCard,1);
    }
    if (this->upIcon == nullptr){
        this->upIcon = Sprite::create("res/up.png");
        this->upIcon->setAnchorPoint(Vec2(0.5,0));
        this->upIcon->setPosition(Vec2(origin.x + visibleSize.width/2,origin.y + visibleSize.height/2 + deckCard->getContentSize().height*0.6));
        addChild(this->upIcon,1);
    }
    
    if (this->downIcon == nullptr){
        this->downIcon = Sprite::create("res/down.png");
        this->downIcon->setAnchorPoint(Vec2(0.5,1));
        this->downIcon->setPosition(Vec2(origin.x + visibleSize.width/2,origin.y + visibleSize.height/2 - deckCard->getContentSize().height*0.6));
        addChild(this->downIcon,1);
    }
    
    
    Size s = deckCard->getContentSize();
    float start_x = origin.x + visibleSize.width / 2 - deckCard->getContentSize().width * 1.25;
    float y_dif = fontSize * 2;
    
    if (this->computerCards.size() == 0) {
        float current_x = start_x;
        float current_y = origin.y + visibleSize.height - y_dif;
        for (int i=0; i < 4; i++) {
            auto cardSprite = GameCardSprite::create();
            cardSprite->setAnchorPoint(Vec2(0,1));
            cardSprite->setPosition(Vec2(current_x,current_y));
            cardSprite->saveDefaultPosition();
            addChild(cardSprite,3 + i);
            cardSprite->setCard(nullptr);
            this->computerCards.push_back(cardSprite);
            current_x += cardSprite->getContentSize().width / 2;
        }
    }
    
    if (this->playerCards.size() == 0) {
        float current_x = start_x;
        float current_y = origin.y + y_dif;
        for (int i=0; i < 4; i++) {
            auto cardSprite = GameCardSprite::create();
            cardSprite->setAnchorPoint(Vec2(0,0));
            cardSprite->setPosition(Vec2(current_x,current_y));
            cardSprite->saveDefaultPosition();
            addChild(cardSprite,10 + i);
            cardSprite->setCard(nullptr);
            this->playerCards.push_back(cardSprite);
            auto listener = EventListenerTouchOneByOne::create();
            listener->setSwallowTouches(true);
            float readyToGoDiff = deckCard->getPosition().y - deckCard->getContentSize().height - (cardSprite->getPosition().y + cardSprite->getContentSize().height);
            readyToGoDiff = readyToGoDiff / 4;
            listener->onTouchBegan = [this, cardSprite, readyToGoDiff, current_x, current_y](Touch* touch, Event* event) {
                if(!cardSprite->isVisible())
                    return false;
                if (!cardSprite->getBoundingBox().containsPoint(touch->getLocation()))
                    return false;
                
                if ( GameData::getInstance()->gameState == GameState::PlayerTurn
                    && cardSprite->isVisible()) {
                    if (cardSprite->getPosition().y > current_y) {
                        GameData::getInstance()->gameState = GameState::PlayerCardDealing;
                        auto anim = MoveTo::create(ANIM_TIME,
                                                   Vec2(this->deckCard->getPosition().x - this->deckCard->getContentSize().width,
                                                        this->deckCard->getPosition().y - this->deckCard->getContentSize().height/2));
                        auto callback = CallFunc::create([this,cardSprite](){
                            GameCard* throwingCard = cardSprite->getCard();
                            this->deckCard->setCard(throwingCard);
                            cardSprite->setVisible(false);
                            cardSprite->loadDefaultPosition();
                            GameData::getInstance()->playerCardPlayed(throwingCard);
                            this->checkGameState(); 
                        });
                        auto seq = Sequence::create(anim,callback, NULL);
                        cardSprite->runAction(seq);
                    } else {
                        for (int i=0; i<this->playerCards.size(); i++) {
                            auto card = this->playerCards.at(i);
                            if (card != cardSprite) {
                                card->loadDefaultPosition();
                            }
                        }
                        auto anim = MoveTo::create(ANIM_TIME/3, Vec2(current_x, current_y + readyToGoDiff));
                        cardSprite->runAction(anim);
                    }
                }
                
                
                return true;
            };
            _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, cardSprite);
            current_x += cardSprite->getContentSize().width / 2;
        }
    }
    
    char buf[100];
    sprintf(buf, "Bilgisayar (%d)",GameData::getInstance()->computerScore);
    computerName->setString(std::string(buf));
    sprintf(buf, "Oyuncu (%d)",GameData::getInstance()->playerScore);
    playerName->setString(std::string(buf));
    
    if (GameData::getInstance()->deckClosedCards.size() > 0){
        closedCard->setVisible(true);
    } else {
        closedCard->setVisible(false);
    }
    
    if (GameData::getInstance()->deckCards.size() > 0) {
        deckCard->setVisible(true);
        deckCard->setCard(GameData::getInstance()->deckCards.back());
    } else {
        deckCard->setVisible(false);
    }
    
    for (int i=0; i < this->playerCards.size(); i++) {
        auto card = this->playerCards.at(i);
        if (i < GameData::getInstance()->playerCards.size()) {
            card->setVisible(true);
            card->setCard(GameData::getInstance()->playerCards.at(i));
        } else {
            card->setVisible(false);
        }
    }
    
    for (int i=0; i < this->computerCards.size(); i++) {
        auto card = this->computerCards.at(i);
        if (i < GameData::getInstance()->computerCards.size()) {
            card->setVisible(true);
        } else {
            card->setVisible(false);
        }
    }
    
    // enum GameState {  , CardDealing, CardsEnd, ComputerWon,PlayerWon};

    switch (GameData::getInstance()->gameState) {
        case GameState::PlayerWon:
        case GameState::PlayerTurn:
        case GameState::PlayerWonHand:
        case GameState::PlayerTurnStart:
        case GameState::PlayerNoCardInHand:
        case GameState::PlayerCardDealing:
            upIcon->setVisible(false);
            downIcon->setVisible(true);
            break;
        case GameState::ComputerWon:
        case GameState::ComputerTurn:
        case GameState::ComputerWonHand:
        case GameState::ComputerTurnStart:
        case GameState::ComputerNoCardInHand:
        case GameState::ComputerCardDealing:
            upIcon->setVisible(true);
            downIcon->setVisible(false);
            break;
        default:
            upIcon->setVisible(false);
            downIcon->setVisible(false);
            break;
    }
    
}

void GameScene::showGameover() {
    
    GameData::getInstance()->clearSaving();
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    this->gameoverLayer = LayerColor::create(Color4B(10, 200, 10, 255));

    float fontSize = this->fontSize * 2;
    
    float current_y = origin.y + visibleSize.height/2 + 3*fontSize;
    auto result = Label::create();
    result->setSystemFontSize(fontSize*2);
    result->setWidth(visibleSize.width*0.9);
    result->setAlignment(TextHAlignment::CENTER);
    result->setAnchorPoint(Vec2(0.5, 0));
    result->setPosition(Vec2(origin.x + visibleSize.width/2, current_y));
    current_y -= fontSize*2.1;
    
    auto playerScore = Label::create();
    playerScore->setSystemFontSize(fontSize);
    playerScore->setAnchorPoint(Vec2(0.5, 1));
    playerScore->setPosition(Vec2(origin.x + visibleSize.width/2, current_y));
    current_y -= fontSize*1.1;
    
    auto computerScore = Label::create();
    computerScore->setSystemFontSize(fontSize);
    computerScore->setAnchorPoint(Vec2(0.5, 1));
    computerScore->setPosition(Vec2(origin.x + visibleSize.width/2, current_y));
    current_y -= fontSize*1.1;
    
    char buf[100];
    sprintf(buf, "Oyuncu %d",GameData::getInstance()->playerScore);
    playerScore->setString(std::string(buf));
    
    sprintf(buf, "Bilgisayar %d",GameData::getInstance()->computerScore);
    computerScore->setString(std::string(buf));
    
    if (GameData::getInstance()->computerScore == GameData::getInstance()->playerScore){
        result->setString(std::string("Berabere"));
    } else if (GameData::getInstance()->computerScore > GameData::getInstance()->playerScore){
        result->setString(std::string("Bilgisayar Kazandı!"));
    } else {
        result->setString(std::string("Oyuncu Kazandı!"));
    }

    current_y -= fontSize;
    
    auto newGameItem = MenuItemFont::create("Yeni Oyun", CC_CALLBACK_1(GameScene::newGame, this));
    newGameItem->setAnchorPoint(Vec2(0.5,1));
    newGameItem->setFontSizeObj(fontSize);
    newGameItem->setIgnoreAnchorPointForPosition(false);
    newGameItem->setPosition(Vec2(origin.x + visibleSize.width/2, current_y ));
    current_y -= fontSize*1.1;
    
    auto goBack = MenuItemFont::create("Ana Menü", CC_CALLBACK_1(GameScene::goBack, this));
    goBack->setAnchorPoint(Vec2(0.5,1));
    goBack->setFontSizeObj(fontSize);
    goBack->setIgnoreAnchorPointForPosition(false);
    goBack->setPosition(Vec2(origin.x + visibleSize.width/2, current_y));
    current_y -= fontSize*1.1;
    
    // create menu, it's an autorelease object
    auto menu = Menu::create(newGameItem,goBack, NULL);
    menu->setPosition(Vec2::ZERO);
    this->gameoverLayer->addChild(menu, 1);
    
    this->gameoverLayer->addChild(playerScore);
    this->gameoverLayer->addChild(computerScore);
    this->gameoverLayer->addChild(result);
    
    addChild(this->gameoverLayer,100);
    gameoverLayer->setOpacity(0);
    
    auto fade = FadeIn::create(2);
    gameoverLayer->runAction(fade);
}

void GameScene::menuTapped(Ref* pSender) {
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    this->menuLayer = LayerColor::create(Color4B(10, 200, 10, 255));
    auto close = MenuItemImage::create("res/close_button.png", "res/close_button.png",CC_CALLBACK_1(GameScene::closeTapped, this));
    close->setAnchorPoint(Vec2(1,1));
    close->setPosition(Vec2(origin.x + visibleSize.width - fontSize, origin.y + visibleSize.height - fontSize));
    
    float fontSize = this->fontSize * 2;
    float current_y = visibleSize.height/2 + fontSize;
    
    auto newGameItem = MenuItemFont::create("Yeni Oyun", CC_CALLBACK_1(GameScene::newGame, this));
    newGameItem->setAnchorPoint(Vec2(0.5,1));
    newGameItem->setFontSizeObj(fontSize);
    newGameItem->setIgnoreAnchorPointForPosition(false);
    newGameItem->setPosition(Vec2(origin.x + visibleSize.width/2, current_y ));
    current_y -= fontSize*1.1;
    
    auto goBack = MenuItemFont::create("Ana Menü", CC_CALLBACK_1(GameScene::goBack, this));
    goBack->setAnchorPoint(Vec2(0.5,1));
    goBack->setFontSizeObj(fontSize);
    goBack->setIgnoreAnchorPointForPosition(false);
    goBack->setPosition(Vec2(origin.x + visibleSize.width/2, current_y));
    current_y -= fontSize*1.1;
    
    
    auto menu = Menu::create(goBack,newGameItem,close, NULL);
    menu->setPosition(Vec2::ZERO);
    this->menuLayer->addChild(menu, 100);
    addChild(this->menuLayer,100);
}

void GameScene::closeTapped(cocos2d::Ref* pSender) {
    this->menuLayer->removeFromParent();
    this->menuLayer = nullptr;
}

void GameScene::newGame(Ref* pSender) {
    if (this->menuLayer != nullptr){
        this->menuLayer->removeFromParent();
        this->menuLayer = nullptr;
    }
    if (this->gameoverLayer != nullptr){
        this->gameoverLayer->removeFromParent();
        this->gameoverLayer = nullptr;
    }
    GameData::getInstance()->newGame();
    renderDeck();
}

void GameScene::goBack(Ref* pSender) {
    auto scene = MenuScene::createScene();
    Director::getInstance()->replaceScene(scene);
}



