#ifndef __GAME_CARD_H__
#define __GAME_CARD_H__

#include "cocos2d.h"
#include <vector>

enum CardColor { Clubs, Diamonds, Hearts, Spades};
enum CardNum { Ace, Card2, Card3, Card4, Card5, Card6, Card7, Card8, Card9, Card10, Jack, Queen, King };

class GameCard
{
    
public:
    CardColor color;
    CardNum num;
    
    GameCard();
    void loadInt(int i);
    int getInt();
};

#endif // __HELLOWORLD_SCENE_H__
