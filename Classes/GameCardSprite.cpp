#include "GameCardSprite.h"
#include "GameCard.h"

USING_NS_CC;

const char *card_names[52] = {
    "res/ace_of_clubs.png",
    "res/2_of_clubs.png",
    "res/3_of_clubs.png",
    "res/4_of_clubs.png",
    "res/5_of_clubs.png",
    "res/6_of_clubs.png",
    "res/7_of_clubs.png",
    "res/8_of_clubs.png",
    "res/9_of_clubs.png",
    "res/10_of_clubs.png",
    "res/jack_of_clubs2.png",
    "res/queen_of_clubs2.png",
    "res/king_of_clubs2.png",
    "res/ace_of_diamonds.png",
    "res/2_of_diamonds.png",
    "res/3_of_diamonds.png",
    "res/4_of_diamonds.png",
    "res/5_of_diamonds.png",
    "res/6_of_diamonds.png",
    "res/7_of_diamonds.png",
    "res/8_of_diamonds.png",
    "res/9_of_diamonds.png",
    "res/10_of_diamonds.png",
    "res/jack_of_diamonds2.png",
    "res/queen_of_diamonds2.png",
    "res/king_of_diamonds2.png",
    "res/ace_of_hearts.png",
    "res/2_of_hearts.png",
    "res/3_of_hearts.png",
    "res/4_of_hearts.png",
    "res/5_of_hearts.png",
    "res/6_of_hearts.png",
    "res/7_of_hearts.png",
    "res/8_of_hearts.png",
    "res/9_of_hearts.png",
    "res/10_of_hearts.png",
    "res/jack_of_hearts2.png",
    "res/queen_of_hearts2.png",
    "res/king_of_hearts2.png",
    "res/ace_of_spades.png",
    "res/2_of_spades.png",
    "res/3_of_spades.png",
    "res/4_of_spades.png",
    "res/5_of_spades.png",
    "res/6_of_spades.png",
    "res/7_of_spades.png",
    "res/8_of_spades.png",
    "res/9_of_spades.png",
    "res/10_of_spades.png",
    "res/jack_of_spades2.png",
    "res/queen_of_spades2.png",
    "res/king_of_spades2.png",
};


bool GameCardSprite::init() {
    Sprite::init();
    this->default_x = 0;
    this->default_y = 0;
    this->setCard(nullptr);
    return true;
}
int GameCardSprite::background_random = -1;


void GameCardSprite::setCard(GameCard* card) {
    this->gameCard = card;
    if (card == nullptr){
        if (background_random != 0 && background_random != 1){
            int rnd = rand()%2;
            background_random = rnd;
        }
        if (background_random == 0){
            setTexture("res/card_back_1.png");
        } else {
            setTexture("res/card_back_2.png");
        }
    } else {
        int i = card->getInt();
        setTexture(card_names[i]);
    }
}

void GameCardSprite::saveDefaultPosition() {
    this->default_x = getPosition().x;
    this->default_y = getPosition().y;
}
void GameCardSprite::loadDefaultPosition() {
    setPosition(Vec2(default_x, default_y));
}

GameCard* GameCardSprite::getCard() {
    return this->gameCard;
}


