#include "MenuScene.h"
#include "GameScene.h"
#include "GameData.h"

USING_NS_CC;



Scene* MenuScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MenuScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}



// on "init" you need to initialize your instance
bool MenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();


    auto newGameItem = MenuItemFont::create("Yeni Oyun", CC_CALLBACK_1(MenuScene::newGame, this));
    newGameItem->setAnchorPoint(Vec2(0.5,0));
    newGameItem->setFontSizeObj(200);
    newGameItem->setIgnoreAnchorPointForPosition(false);
    newGameItem->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2 ));
    
    auto continueGameItem = MenuItemFont::create("Devam", CC_CALLBACK_1(MenuScene::continueGame, this));
    continueGameItem->setFontSizeObj(200);
    continueGameItem->setAnchorPoint(Vec2(0.5,1));
    continueGameItem->setIgnoreAnchorPointForPosition(false);
    continueGameItem->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2 ));
    
    // create menu, it's an autorelease object
    auto menu = Menu::create(newGameItem,continueGameItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
    return true;
}


void MenuScene::newGame(Ref* pSender) {
    GameData::getInstance()->clearSaving();
    auto scene = GameScene::createScene();
    Director::getInstance()->replaceScene(scene);
}

void MenuScene::continueGame(Ref* pSender) {
    auto scene = GameScene::createScene();
    Director::getInstance()->replaceScene(scene);
}
