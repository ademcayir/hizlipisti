#ifndef __GAME_DATA_H__
#define __GAME_DATA_H__

#include "cocos2d.h"
#include <vector>

class GameCard;

enum GameState { PlayerTurn,ComputerTurn,PlayerTurnStart, ComputerTurnStart, PlayerNoCardInHand, ComputerNoCardInHand, PlayerWonHand, ComputerWonHand, PlayerCardDealing, ComputerCardDealing, CardDealing, CardsEnd, ComputerWon,PlayerWon};

class GameData
{
private:
    static GameData* instance;
    GameData();
    void clearVectors();
    int checkDeckCard();
public:
    static GameData* getInstance();
    
    std::vector<GameCard*> deckClosedCards;
    std::vector<GameCard*> deckCollectedCards;
    std::vector<GameCard*> playerCards;
    std::vector<GameCard*> computerCards;
    std::vector<GameCard*> deckCards;
    GameState gameState;
    int playerScore;
    int computerScore;
    
    void loadGame();
    void newGame();
    void saveState();
    void clearSaving();

    void playerCardPlayed(GameCard* card);
    GameCard* computerCard();
    void computerCardPlayed(GameCard* card);
    void nextState();
};

#endif // __HELLOWORLD_SCENE_H__
