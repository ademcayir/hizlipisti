#ifndef __GAME_CARD_SPRITE_H__
#define __GAME_CARD_SPRITE_H__

#include "cocos2d.h"
#include <vector>

/*
 
 enum CardColor { Clubs, Diamonds, Hearts, Spades};
 enum CardNum { Ace, Card2, Card3, Card4, Card5, Card6, Card7, Card8, Card9, Card10, Jack, Queen, King };
*/
extern const char* card_names[52];

class GameCard;

class GameCardSprite : public cocos2d::Sprite
{
private:
    GameCard* gameCard;
    float default_x;
    float default_y;
    static int background_random;
public:
    virtual bool init();
    void setCard(GameCard* card);
    GameCard* getCard();
    void saveDefaultPosition();
    void loadDefaultPosition();
    CREATE_FUNC(GameCardSprite);
};

#endif // __HELLOWORLD_SCENE_H__
