#include "GameCard.h"

USING_NS_CC;



GameCard::GameCard()
{
    loadInt(0);
}
void GameCard::loadInt(int i)
{
    if (i >= 0 && i < 52){
        int color = i / 13;
        int num = i % 13;
        this->color = (CardColor)color;
        this->num = (CardNum)num;
    }
}

int GameCard::getInt()
{
    return (int)this->color * 13 + (int)this->num;
}
