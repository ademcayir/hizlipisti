#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"

class GameCardSprite;

class GameScene : public cocos2d::Layer
{
private:
    cocos2d::Label* computerName;
    cocos2d::Label* playerName;
    std::vector<GameCardSprite*> computerCards;
    std::vector<GameCardSprite*> playerCards;
    cocos2d::Layer* gameoverLayer;
    GameCardSprite* deckCard;
    GameCardSprite* closedCard;
    float fontSize;
    void showGameover();
    void renderDeck();
    void checkGameState();
    void newGame(cocos2d::Ref* pSender);
    void goBack(cocos2d::Ref* pSender);
    void menuTapped(cocos2d::Ref* pSender);
    void closeTapped(cocos2d::Ref* pSender);
    cocos2d::MenuItemImage* menuItem;
    cocos2d::Layer* menuLayer;
    cocos2d::Sprite* downIcon;
    cocos2d::Sprite* upIcon;

public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameScene);
    
};

#endif // __HELLOWORLD_SCENE_H__
